/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intsllnodo;

/**
 *
 * @author Usuario
 */
public class IntSLLNodo { //SLL = Single Linked List

    //IntSLList
    public static void main(String[] args) {
       //EN ESTAS LISTAS SOLO APUNTAN A LA DERECHA
       //NO TIENEN LA REFERENCIA DEL ELEMENTO ANTERIOR
       
        
        IntSLList lista;
        lista = new IntSLList();
        System.out.println(lista.isEmpty());
        
        
        lista.addToHead(8);
        lista.printAll();
        System.out.println();
        lista.addToHead(3);
        lista.printAll();
        System.out.println();
        lista.addToTail(2);
        lista.printAll();
        System.out.println();
        lista.delete(8);
        lista.printAll();
        System.out.println();
        
        System.out.println(lista.isEmpty());
         
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intsllnodo;


public class IntSLList { //Clase = Lista simplemente enlazada
    
    protected IntSLLNodoClass head; //Nodo cabecera
    protected IntSLLNodoClass tail; //Nodo final
    
    public IntSLList(){  //Constructor
        head = tail = null;
    }
    
    public boolean isEmpty(){   //Siempre los metodos booleanos inician con IS en el prefijo
        return head == null;
    }
    
    public void addToHead(int el){
        head = new IntSLLNodoClass(el, head);
        if(tail == null){
            tail = head;
        }
    }
    
    public void addToTail(int el){
        if (!isEmpty()) {
            tail.next = new IntSLLNodoClass(el);
            tail = tail.next;
        }else{
            head = tail = new IntSLLNodoClass(el);
        }
      
    }
    
    public int deleteFromHead(){ //Elimina nodo HEAD y da el valor que eliminaste
                                // Es decir el atirbuto info
        int el = head.info;
        if (head == tail) {  //Pregunta si hay un solo nodo en la lista
            head = tail = null;
        } else{
            head = head.next;  //Head.next significa el segundo modo que nodo head
                      //sera el segundo nodo de la lista
        }
        return el;
    }
    
    public int deleteFromTail(){ //Elimina el ultimo nodo de la lista Enlazada
        int el = tail.info;
        if (head == tail) { //Si es verdad entonces solo hay un nodo en la SSL
            head = tail = null;
        }else {  //Si hay mas de un nodo hace lo siguiente
            IntSLLNodoClass tmp;  //Para guardar el predecesor del nodo tail
            for ( tmp = head; tmp.next != tail; tmp = tmp.next) { //Guardamos en tmp la dirección del nodo
                tail = tmp; //El predecesor del nodo tail ahora es el nodo tail
                tail.next = null;
            }
        }
        return el;
    }
    
    public void printAll(){
        for (IntSLLNodoClass tmp = head;  tmp != null; tmp = tmp.next) {
            System.out.print(tmp.info + "   " );
        }
    }
    
    public boolean isInList(int el){
        IntSLLNodoClass tmp;
        for (tmp = head; tmp != null && tmp.info != el; tmp = tmp.next) {
            
        }
        return tmp != null;
    }
    
    public void delete(int el){ //Elimina el primer nodo con el elemento "el"
        if (!isEmpty()) {
            if (head == tail && head.info == el) { //Si solo hay un nodo en la SLL
                head = tail = null;
            }else if (el == head.info){ //Si hay más de un nodo en la SLL
                head = head.next; //Se borró el nodo con el elemento
            }else{
                IntSLLNodoClass pred, tmp;  //Si el elemento no está en el nodo head
                for ( pred = head, tmp = head.next; 
                        tmp != null && tmp.info != el; 
                        pred = pred.next, tmp = tmp.next) {
                    
                }
                if (tmp != null) {  //El elemento "el" se encontro
                    pred.next = tmp.next;
                    if (tmp == tail) { //Preguntamos ei el elemento se hallo en tail
                        tail = pred;
                    }
                }
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intsllnodo;

/**
 *
 * @author Usuario
 */
public class IntSLLNodoClass {
    public int info;    //Tipo de dato a almacenar en el nodo
    public IntSLLNodoClass next;  //Dirección de memoria del siguiente nodo
    
    public IntSLLNodoClass(int i){
        this(i,null);
    }
    
    public IntSLLNodoClass(int i, IntSLLNodoClass n){
        info = i;
        next = n;
    }
}
